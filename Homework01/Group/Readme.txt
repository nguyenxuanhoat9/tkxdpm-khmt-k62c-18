Nhóm 18 phân công công việc cho Homework01:

Nhóm 18 gồm các sinh viên: 

Nguyễn Văn Hồng	 20173146
Nguyễn Sỹ Trọng	 20173414
Ngô Huy Thao	 20173381
Nguyễn Xuân Hoạt 20173144
Nông Thanh Đạt	 20173004
Trần Xuân Đức	 20173034
Đặng Trung Kiên	 20173210

Phân công công việc:

Nguyễn Văn Hồng		Use case: Xem thông tin bãi xe
Nguyễn Xuân Hoạt	Use case: Thêm bãi xe 
Ngô Huy Thao		Use case: Thêm xe mới (Admin)
Nguyễn Sỹ Trọng		Use case: Trả xe
Nông Thanh Đạt		Use case: Xem danh sách bãi xe
Trần Xuân Đức		Use case: Thuê xe
Đặng Trung Kiên		Use case: Quản lý xe đang sử dụng (Admin)

