/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.oms.components.abstractdata.gui;

import com.oms.components.abstractdata.controller.IDataManageController;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 *
 * @author kienvipmkvn
 */
public abstract class ADataCommonDialog<T> extends JDialog  {
    protected T t;
    protected GridBagLayout layout;
    protected GridBagConstraints c = new GridBagConstraints();

    public ADataCommonDialog(T t, String dialogName) {
        super((Frame) null, dialogName, true);

        this.t = t;

        setContentPane(new JPanel());
        layout = new GridBagLayout();
        getContentPane().setLayout(layout);

        this.buildControls();

        this.pack();
        this.setResizable(false);
        this.setVisible(true);
    }

    protected int getLastRowIndex() {
        layout.layoutContainer(getContentPane());
        int[][] dim = layout.getLayoutDimensions();
        int rows = dim[1].length;
        return rows;
    }

    public abstract void buildControls();
}
