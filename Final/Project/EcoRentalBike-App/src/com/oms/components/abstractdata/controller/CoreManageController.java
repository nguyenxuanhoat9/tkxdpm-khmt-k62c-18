package com.oms.components.abstractdata.controller;
import com.oms.bean.Core;
import com.oms.components.core.controller.AAdminCorePageController;


public class CoreManageController implements IDataManageController<Core> {

	private AAdminCorePageController controller;
	
	public CoreManageController(AAdminCorePageController controller) {
		this.controller = controller; 
	}

	@Override
	public void update(Core t) {
		
	}

	@Override
	public void create(Core t) {
		System.out.println("abc");
		controller.createCore(t);
	}

	@Override
	public void read(Core t) {
	}

	@Override
	public void delete(Core t) {
		
	}
}
