package com.oms.components.core.controller;

import com.oms.bean.Core;
import com.oms.components.abstractdata.controller.AUserPageController;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.UserListPane;
import com.oms.components.core.gui.UserCoreListPane;

public abstract class UserCorePageController extends AUserPageController<Core> {
	public UserCorePageController() {
		super();
	}
	
	@Override
	public UserListPane<Core> createListPane() {
		return new UserCoreListPane(this);
	}
	
	public abstract Core updateCore(Core core);
	
	public abstract Core createCore(Core core);
}