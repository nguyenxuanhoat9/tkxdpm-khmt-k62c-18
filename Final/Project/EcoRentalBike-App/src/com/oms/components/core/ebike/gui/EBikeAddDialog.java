package com.oms.components.core.ebike.gui;

import com.oms.bean.EBike;
import com.oms.bean.Core;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.Core;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataAddDialog;
import com.oms.components.core.gui.BikeAddDialog;

public class EBikeAddDialog extends ADataAddDialog<Core> {

	private EBike ebike;
	private JTextField nameField;
	private JTextField idField;
	private JTextField costField;
	private JTextField weightField;
	private JTextField licenseField;
	private JTextField manufacturingField;
	private JTextField producerField;
	private JTextField batteryField;
	private JTextField loadField;
	private JTextField estimateField;

	
	public EBikeAddDialog(Core core, IDataManageController<Core> controller) {
		super(core,controller);
		
	}
	
	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel titleLabel = new JLabel("Name");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(titleLabel, c);
		nameField = new JTextField(15);
		add(titleLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(nameField, c);
		
		row = getLastRowIndex();
		JLabel idLabel = new JLabel("id");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(idLabel, c);
		idField = new JTextField(15);
		add(idLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(idField, c);
		
		row = getLastRowIndex();
		JLabel weightLabel = new JLabel("Weight");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(weightLabel, c);
		weightField = new JTextField(15);
		add(weightLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(weightField, c);

		row = getLastRowIndex();		
		JLabel mfLabel = new JLabel("Manufacrturing Date");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(mfLabel, c);
		manufacturingField = new JTextField(15);
		add(mfLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(manufacturingField, c);

		row = getLastRowIndex();		
		JLabel licenseLabel = new JLabel("LiencePlate");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(licenseLabel, c);
		licenseField = new JTextField(15);
		add(licenseLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(licenseField, c);
		
		row = getLastRowIndex();
		JLabel costLabel = new JLabel("Cost");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(costLabel, c);
		costField = new JTextField(15);
		add(costLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(costField, c);
		
		row = getLastRowIndex();
		JLabel producerLabel = new JLabel("Producer");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(producerLabel, c);
		producerField = new JTextField(15);
		add(producerLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(producerField, c);
		
		
		row = getLastRowIndex();
		JLabel batteryLabel = new JLabel("Battery");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(batteryLabel, c);
		batteryField = new JTextField(15);
		add(batteryLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(batteryField, c);
		
		row = getLastRowIndex();
		JLabel loadLabel = new JLabel("LoadCycles");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(loadLabel, c);
		loadField = new JTextField(15);
		add(loadLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(loadField, c);

		row = getLastRowIndex();
		JLabel estimateLabel = new JLabel("Estimate Time");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(estimateLabel, c);
		estimateField = new JTextField(15);
		add(estimateLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(estimateField, c);
		
	}

	@Override
	public Core createNewData() {
		ebike.setName(nameField.getText());
		ebike.setCost(Float.parseFloat(costField.getText()));
		ebike.setLicensePlate(licenseField.getText());
		ebike.setId(idField.getText());
		ebike.setProducer(producerField.getText());
		ebike.setWeight(Float.parseFloat(weightField.getText()));
		String mfDate = manufacturingField.getText();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		
        Date date = null;
		try {
			date = formatter.parse(mfDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        ebike.setManufacturingDate(date);

       
		return ebike;
	}
	
}
