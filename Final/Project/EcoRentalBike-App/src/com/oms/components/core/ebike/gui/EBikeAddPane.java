package com.oms.components.core.ebike.gui;

import com.oms.bean.EBike;
import com.oms.components.abstractdata.controller.CoreManageController;
import com.oms.components.abstractdata.gui.ADataAddDialog;
import com.oms.components.core.gui.BikeAddDialog;
import com.oms.components.core.gui.CoreAddPane;

public class EBikeAddPane extends CoreAddPane {

	private EBike ebike;
	
	private CoreManageController controller;
	
	public EBikeAddPane(CoreManageController controller) {
		super(controller);
	}
	
	@Override
	public ADataAddDialog display() {
		return new EBikeAddDialog(ebike, controller);
	}
	
}
