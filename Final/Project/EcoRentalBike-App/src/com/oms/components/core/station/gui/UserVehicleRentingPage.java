/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.oms.components.core.station.gui;

import com.oms.bean.Core;
import com.oms.bean.EBike;
import com.oms.components.abstractdata.gui.ADataCommonDialog;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 *
 * @author kienvipmkvn
 */
public class UserVehicleRentingPage  extends ADataCommonDialog{
    public UserVehicleRentingPage(Core core, String dialogName) {
        super(core, dialogName);
    }

    @Override
    public void buildControls() {
        if (t instanceof EBike) {
            EBike eBike = (EBike) t;
            JButton btnBack = new JButton("Back");
            btnBack.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
            JLabel nameLabel = new JLabel("User vehicle renting page                   ".toUpperCase());
            c.fill = GridBagConstraints.HORIZONTAL;
            int row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(nameLabel, c);
            
            c.gridx = 2;
            getContentPane().add(btnBack, c);row = getLastRowIndex();
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel(" "), c);
            
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("Information"), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        ID: " + eBike.getId()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Name: " + eBike.getName()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Plate: " + eBike.getLicensePlate()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Cost: " + eBike.getCost() + "VND"), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Estimated usage time remaining: " + eBike.getEstimatedUsageTimeRemaining() + "min"), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Producer: " + eBike.getProducer()), c);
        }
    }
}
