package com.oms.components.core.gui;

import java.awt.event.ActionEvent;

import com.oms.bean.*;
import com.oms.components.abstractdata.controller.CoreManageController;
import com.oms.components.abstractdata.gui.ADataAddDialog;

public class BikeAddPane extends CoreAddPane {

	private Bike bike = new Bike("", "");

	public BikeAddPane(CoreManageController controller) {
		super(controller);
		System.out.println("BikeAddPane controller = " + controller);
	}

	@Override
	public ADataAddDialog display() {
		return new BikeAddDialog(bike, controller);
	}

}
