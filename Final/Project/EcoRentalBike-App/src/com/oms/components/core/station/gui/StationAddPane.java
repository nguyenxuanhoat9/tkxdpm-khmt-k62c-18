package com.oms.components.core.station.gui;

import com.oms.bean.Station;
import com.oms.components.abstractdata.controller.CoreManageController;
import com.oms.components.abstractdata.gui.ADataAddDialog;
import com.oms.components.core.gui.BikeAddDialog;
import com.oms.components.core.gui.CoreAddPane;

public class StationAddPane extends CoreAddPane {

	private Station station = new Station("","","");
	
	
	
	public StationAddPane(CoreManageController controller) {
		super(controller);
	}
	
	@Override
	public ADataAddDialog display() {
		return new StationAddDialog(station, controller);
	}
	
}
