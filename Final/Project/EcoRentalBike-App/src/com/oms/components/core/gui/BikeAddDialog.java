package com.oms.components.core.gui;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import com.oms.bean.*;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.oms.components.abstractdata.controller.CoreManageController;
import com.oms.components.abstractdata.gui.ADataAddDialog;


public class BikeAddDialog extends CoreAddDialog{
	
	private JTextField costField;
	private JTextField weightField;
	private JTextField licenseField;
	private JTextField manufacturingField;
	private JTextField producerField;
	
	public BikeAddDialog(Core core, CoreManageController controller) {
		super(core, controller);
		System.out.println("BikeAddDialog controller = " + controller);
	}
	
	@Override
	public void buildControls() {
		super.buildControls();
		
		if (t instanceof Bike) {
			Bike book = (Bike) t;
			
			int row = getLastRowIndex();
			
			JLabel mfLabel = new JLabel("Manufacrturing Date");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(mfLabel, c);
			manufacturingField = new JTextField(15);
			add(mfLabel, c);
			c.gridx = 1;
			c.gridy = row;
			add(manufacturingField, c);
			row = getLastRowIndex();
			
			JLabel licenseLabel = new JLabel("LiencePlate");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(licenseLabel, c);
			licenseField = new JTextField(15);
			add(licenseLabel, c);
			c.gridx = 1;
			c.gridy = row;
			add(licenseField, c);
			row = getLastRowIndex();
			
			JLabel costLabel = new JLabel("Cost");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(costLabel, c);
			costField = new JTextField(15);
			add(costLabel, c);
			c.gridx = 1;
			c.gridy = row;
			add(costField, c);
			row = getLastRowIndex();
			
			JLabel producerLabel = new JLabel("Producer");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(producerLabel, c);
			producerField = new JTextField(15);
			add(producerLabel, c);
			c.gridx = 1;
			c.gridy = row;
			add(producerField, c);
			
			
			row = getLastRowIndex();
			JLabel languageLabel = new JLabel("Weight");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(languageLabel, c);
			weightField = new JTextField(15);
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(weightField, c);
		}
	}

	@Override
	public Core createNewData() {
		super.createNewData();
		
		if (t instanceof Bike) {
			Bike bike = (Bike) t;
			
			bike.setCost(Float.parseFloat(costField.getText()));
			bike.setWeight(Float.parseFloat(weightField.getText()));
			bike.setLicensePlate(licenseField.getText());
			bike.setProducer(producerField.getText());
		}
		
		return t;
	}
}

//@SuppressWarnings("serial")
//public class BikeAddDialog extends ADataAddDialog<Core>{
//	
//	private JTextField nameField;
//	private JTextField idField;
//	private JTextField costField;
//	private JTextField weightField;
//	private JTextField licenseField;
//	private JTextField manufacturingField;
//	private JTextField producerField;
//	
//	
//	public BikeAddDialog(Core core, IDataManageController<Core> controller) {
//		super(core, controller);
//	}
//
//	@Override
//	public void buildControls() {
//		int row = getLastRowIndex();
//		JLabel titleLabel = new JLabel("Name");
//		c.gridx = 0;
//		c.gridy = row;
//		getContentPane().add(titleLabel, c);
//		nameField = new JTextField(15);
//		add(titleLabel, c);
//		c.gridx = 1;
//		c.gridy = row;
//		add(nameField, c);
//		
//		row = getLastRowIndex();
//		JLabel idLabel = new JLabel("id");
//		c.gridx = 0;
//		c.gridy = row;
//		getContentPane().add(idLabel, c);
//		idField = new JTextField(15);
//		add(idLabel, c);
//		c.gridx = 1;
//		c.gridy = row;
//		add(idField, c);
//		row = getLastRowIndex();
//		
//		JLabel weightLabel = new JLabel("Weight");
//		c.gridx = 0;
//		c.gridy = row;
//		getContentPane().add(weightLabel, c);
//		weightField = new JTextField(15);
//		add(weightLabel, c);
//		c.gridx = 1;
//		c.gridy = row;
//		add(weightField, c);
//		row = getLastRowIndex();
//		
//		JLabel mfLabel = new JLabel("Manufacrturing Date");
//		c.gridx = 0;
//		c.gridy = row;
//		getContentPane().add(mfLabel, c);
//		manufacturingField = new JTextField(15);
//		add(mfLabel, c);
//		c.gridx = 1;
//		c.gridy = row;
//		add(manufacturingField, c);
//		row = getLastRowIndex();
//		
//		JLabel licenseLabel = new JLabel("LiencePlate");
//		c.gridx = 0;
//		c.gridy = row;
//		getContentPane().add(licenseLabel, c);
//		licenseField = new JTextField(15);
//		add(licenseLabel, c);
//		c.gridx = 1;
//		c.gridy = row;
//		add(licenseField, c);
//		row = getLastRowIndex();
//		
//		JLabel costLabel = new JLabel("Cost");
//		c.gridx = 0;
//		c.gridy = row;
//		getContentPane().add(costLabel, c);
//		costField = new JTextField(15);
//		add(costLabel, c);
//		c.gridx = 1;
//		c.gridy = row;
//		add(costField, c);
//		row = getLastRowIndex();
//		
//		JLabel producerLabel = new JLabel("Producer");
//		c.gridx = 0;
//		c.gridy = row;
//		getContentPane().add(producerLabel, c);
//		producerField = new JTextField(15);
//		add(producerLabel, c);
//		c.gridx = 1;
//		c.gridy = row;
//		add(producerField, c);
//		
//	}
//
//	@Override
//	public Core createNewData() {
//
//		Bike bike = (Bike) t ;
//		bike.setName(nameField.getText());
//		bike.setCost(Float.parseFloat(costField.getText()));
//		bike.setLicensePlate(licenseField.getText());
//		bike.setId(idField.getText());
//		bike.setProducer(producerField.getText());
//		bike.setWeight(Float.parseFloat(weightField.getText()));
////		String mfDate = manufacturingField.getText();
////		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
////		
////        Date date = null;
////		try {
////			date = formatter.parse(mfDate);
////		} catch (ParseException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
////		bike.setManufacturingDate(date);
//        System.out.println("day la "+ bike);
//       
//		return bike;
//		
//	}
//}