/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.oms.components.core.gui;

import com.oms.bean.Bike;
import com.oms.bean.Core;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataViewDialog;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author kienvipmkvn
 */
@SuppressWarnings("serial")
public class CoreViewDialog extends ADataViewDialog<Core>{
	public CoreViewDialog(Core core, IDataManageController<Core> controller, String dialogName) {
		super(core, controller, dialogName);
	}
	
	@Override
	public void buildControls() {
	
	}
}
