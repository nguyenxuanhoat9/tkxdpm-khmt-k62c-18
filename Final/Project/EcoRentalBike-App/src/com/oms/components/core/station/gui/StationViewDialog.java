package com.oms.components.core.station.gui;

import com.oms.bean.Bike;
import com.oms.bean.Core;
import com.oms.bean.EBike;
import com.oms.bean.Station;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataViewDialog;
import com.oms.components.core.gui.CoreEditDialog;
import com.oms.components.core.gui.CoreViewDialog;
import com.oms.serverapi.CoreApi;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class StationViewDialog extends CoreViewDialog {

    public StationViewDialog(Core core, IDataManageController<Core> controller, String dialogName) {
        super(core, controller, dialogName);
    }

    @Override
    public void buildControls() {
        super.buildControls();
        if (t instanceof Station) {
            Station station = (Station) t;
            JButton btnBack = new JButton("Back");
            btnBack.addActionListener((ActionEvent e) -> {
                dispose();
            });
            JLabel nameLabel = new JLabel("User station page                   ".toUpperCase());
            c.fill = GridBagConstraints.HORIZONTAL;
            int row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(nameLabel, c);

            c.gridx = 8;
            c.gridwidth = 2;
            getContentPane().add(btnBack, c);

            //thong tin station
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel("Station: " + station.getName()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel("        ID: " + station.getId()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel("        Location: " + station.getAddress()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel("        Number of vehicles: " + station.getNumberOfBikes()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel(" "), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 4;
            getContentPane().add(new JLabel("  ID                   "), c);

            c.gridx = 4;
            getContentPane().add(new JLabel("  Name"), c);

            CoreApi api = new CoreApi();
            //lay danh sach xe tu api
            for (int i = 0; i < station.getListBikes().size(); i++) {
                c.gridwidth = 4;
                String bikeId = station.getListBikes().get(i);
                HashMap<String, String> map = new HashMap<>();
                map.put("id", bikeId);

                EBike eBike = api.getEBikes(map).get(0);

                row = getLastRowIndex();
                c.gridx = 0;
                c.gridy = row;
                getContentPane().add(new JLabel("  " + eBike.getId()), c);
                c.gridx = 4;
                getContentPane().add(new JLabel("  " + eBike.getName()), c);

                JButton btnView = new JButton("View");
                btnView.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new EBikeInformationDialog(eBike, "Vehicle information");
                    }
                });

                c.gridwidth = 2;
                c.gridx = 8;
                getContentPane().add(btnView, c);
                c.gridx = 10;
                JButton btnRent = new JButton("Rent");
                btnRent.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dispose();
                        new UserRentPageDialog(eBike, "User rent page");
                    }
                });

                getContentPane().add(btnRent, c);
            }

            c.gridwidth = 8;
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel(" "), c);

//            row = getLastRowIndex();
//            c.gridwidth = 4;
//            c.gridx = 0;
//            c.gridy = row;
//            getContentPane().add(new JLabel("Vehicle ID: "), c);
//
//            c.gridx = 4;
//            JTextField tfVehicleId = new JTextField(20);
//            getContentPane().add(tfVehicleId, c);
//
//            row = getLastRowIndex();
//            c.gridx = 0;
//            c.gridy = row;
//            getContentPane().add(new JLabel(" "), c);
//            JButton btnRent = new JButton("Rent");
//            btnRent.addActionListener(new ActionListener() {
//
//                @Override
//                public void actionPerformed(ActionEvent e) {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("id", tfVehicleId.getText());
//                    ArrayList<EBike> eBikes = api.getEBikes(map);
//                    if (eBikes.size() == 1) {
//                        new UserRentPageDialog(eBikes.get(0), "User rent page");
//                    }
//                }
//            });
//
//            c.gridx = 8;
//            getContentPane().add(btnRent, c);
        }
    }
}
