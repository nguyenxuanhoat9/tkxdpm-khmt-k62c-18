package com.oms.components.core.station.gui;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.oms.bean.Bike;
import com.oms.bean.Core;
import com.oms.bean.EBike;
import com.oms.components.abstractdata.gui.ADataCommonDialog;

/**
 * 
 * @author Hongbeubeu
 *
 */
public class BikeInformationDialog extends ADataCommonDialog{
	public BikeInformationDialog(Core core, String dialogName) {
		super(core, dialogName);
	}
	
	@Override
	public void buildControls() {
		if (t instanceof Bike) {
            Bike bike = (Bike) t;
            JButton btnBack = new JButton("Back");
            btnBack.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
            JLabel nameLabel = new JLabel("User vehicle renting page                   ".toUpperCase());
            c.fill = GridBagConstraints.HORIZONTAL;
            int row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(nameLabel, c);
            
            c.gridx = 2;
            getContentPane().add(btnBack, c);row = getLastRowIndex();
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel(" "), c);
            
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("Information"), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        ID: " + bike.getId()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Name: " + bike.getName()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Plate: " + bike.getLicensePlate()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Cost: " + bike.getCost() + "VND"), c);

           
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Producer: " + bike.getProducer()), c);

            row = getLastRowIndex();
            
            JButton btnRent = new JButton("Rent");
            btnRent.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                    new UserRentPageDialog(bike, "User rent page");
                }
            });
            
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(btnRent, c);
		}
	}

}
