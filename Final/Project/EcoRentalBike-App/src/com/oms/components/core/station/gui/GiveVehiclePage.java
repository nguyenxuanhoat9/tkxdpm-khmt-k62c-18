/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.oms.components.core.station.gui;

import com.oms.bean.Core;
import com.oms.bean.EBike;
import com.oms.components.abstractdata.gui.ADataCommonDialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author kienvipmkvn
 */
public class GiveVehiclePage  extends ADataCommonDialog{
    public GiveVehiclePage(Core core, String dialogName) {
        super(core, dialogName);
    }

    @Override
    public void buildControls() {
        if (t instanceof EBike) {
            EBike eBike = (EBike) t;
            
            JLabel nameLabel = new JLabel("Give vehicle page".toUpperCase());
            c.fill = GridBagConstraints.HORIZONTAL;
            int row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(nameLabel, c);
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel(" "), c);
                    
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel("Vehicle ID: " + eBike.getId()), c);
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel("Credit card code: "), c);
            
            c.gridx = 8;
            c.gridwidth = 8;
            getContentPane().add(new JTextField(20), c);
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel("Choose station: "), c);
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel(" "), c);

            row = getLastRowIndex();
            JButton btnOk = new JButton("OK");
            btnOk.setPreferredSize(new Dimension(100, 30));
            
            btnOk.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
            
            JButton btnCancel = new JButton("Cancel");
            btnCancel.setPreferredSize(new Dimension(100, 30));
            btnCancel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
            c.gridwidth = 8;
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(btnOk, c);
            c.gridwidth = 4;
            c.gridx = 8;
            getContentPane().add(btnCancel, c);
        }
    }
}
