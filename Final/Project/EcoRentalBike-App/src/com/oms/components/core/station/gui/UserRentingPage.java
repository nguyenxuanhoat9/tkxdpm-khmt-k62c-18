/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.oms.components.core.station.gui;

import com.oms.bean.Core;
import com.oms.bean.EBike;
import com.oms.components.abstractdata.gui.ADataCommonDialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author kienvipmkvn
 */
public class UserRentingPage extends ADataCommonDialog{
    public UserRentingPage(Core core, String dialogName) {
        super(core, dialogName);
    }

    @Override
    public void buildControls() {
        if (t instanceof EBike) {
            EBike eBike = (EBike) t;
            
            JLabel nameLabel = new JLabel("User renting page".toUpperCase());
            c.fill = GridBagConstraints.HORIZONTAL;
            int row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(nameLabel, c);
            
            c.gridx = 8;
            c.gridy = row;
            c.gridwidth = 2;
            getContentPane().add(new JButton("Balance"), c);
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel(" "), c);
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            JButton btnInfor = new JButton("View vehicle information");
            btnInfor.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    new UserVehicleRentingPage(eBike, "User vehicle renting page");
                }
            });
            getContentPane().add(btnInfor, c);
                    
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel("Vehicle ID: " + eBike.getId()), c);
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel("Vehicle name: " + eBike.getName()), c);
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel("Time: 30'"), c);
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            c.gridwidth = 8;
            getContentPane().add(new JLabel("Price: 30$"), c);
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel(" "), c);

            row = getLastRowIndex();
            
            c.gridwidth = 8;
            JButton btnPay = new JButton("Give vehicle to station");
            btnPay.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                    new GiveVehiclePage(eBike, "Give vehicle page");
                }
            });
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(btnPay, c);
        }
    }
}
