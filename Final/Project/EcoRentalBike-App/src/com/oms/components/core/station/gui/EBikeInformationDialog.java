/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.oms.components.core.station.gui;

import com.oms.bean.Core;
import com.oms.bean.EBike;
import com.oms.bean.Station;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataCommonDialog;
import com.oms.serverapi.CoreApi;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 *
 * @author kienvipmkvn
 */
public class EBikeInformationDialog extends ADataCommonDialog{
    public EBikeInformationDialog(Core core, String dialogName) {
        super(core, dialogName);
    }

    @Override
    public void buildControls() {
        if (t instanceof EBike) {
            EBike eBike = (EBike) t;
            JButton btnBack = new JButton("Back");
            btnBack.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
            JLabel nameLabel = new JLabel("User vehicle renting page                   ".toUpperCase());
            c.fill = GridBagConstraints.HORIZONTAL;
            int row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(nameLabel, c);
            
            c.gridx = 2;
            getContentPane().add(btnBack, c);row = getLastRowIndex();
            
            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel(" "), c);
            
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("Information"), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        ID: " + eBike.getId()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Name: " + eBike.getName()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Plate: " + eBike.getLicensePlate()), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Cost: " + eBike.getCost() + "VND"), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Estimated usage time remaining: " + eBike.getEstimatedUsageTimeRemaining() + "min"), c);

            row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(new JLabel("        Producer: " + eBike.getProducer()), c);

            row = getLastRowIndex();
            
            JButton btnRent = new JButton("Rent");
            btnRent.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                    new UserRentPageDialog(eBike, "User rent page");
                }
            });
            
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(btnRent, c);
        }
    }
}
