package com.oms.components.core.station.gui;

import com.oms.bean.Bike;
import com.oms.bean.Core;
import com.oms.bean.Station;
import com.oms.components.abstractdata.controller.CoreManageController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataAddDialog;
import com.oms.components.core.gui.BikeAddDialog;
import com.oms.components.core.gui.CoreAddDialog;
import com.oms.components.core.gui.CoreAddPane;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTextField;

//public class StationAddDialog extends ADataAddDialog<Core> {
//
//	private Station station;
//	private JTextField nameField;
//	private JTextField idField;
//	private JTextField addressField;
//
//
//	
//	public StationAddDialog(Core core, IDataManageController<Core> controller) {
//		super(core, controller);
//		
//	}
//	
//	@Override
//	public void buildControls() {
//		int row = getLastRowIndex();
//		JLabel titleLabel = new JLabel("Name");
//		c.gridx = 0;
//		c.gridy = row;
//		getContentPane().add(titleLabel, c);
//		nameField = new JTextField(15);
//		add(titleLabel, c);
//		c.gridx = 1;
//		c.gridy = row;
//		add(nameField, c);
//		
//		row = getLastRowIndex();
//		JLabel idLabel = new JLabel("id");
//		c.gridx = 0;
//		c.gridy = row;
//		getContentPane().add(idLabel, c);
//		idField = new JTextField(15);
//		add(idLabel, c);
//		c.gridx = 1;
//		c.gridy = row;
//		add(idField, c);
//
//
//		
//		row = getLastRowIndex();
//		JLabel addLabel = new JLabel("Address");
//		c.gridx = 0;
//		c.gridy = row;
//		getContentPane().add(addLabel, c);
//		addressField = new JTextField(15);
//		add(addLabel, c);
//		c.gridx = 1;
//		c.gridy = row;
//		add(addressField, c);
//		
//		
//	}
//
//	@Override
//	public Core createNewData() {
//		station.setName(nameField.getText());
//		station.setId(idField.getText());
//		station.setAddress(addressField.getText());
//       
//		return station;
//	}
//	
//}


public class StationAddDialog extends CoreAddDialog{
	
	private JTextField addressField;
	private JTextField numOfBikesField;
	private JTextField numOfTwinBikesField;
	private JTextField numOfEBikesField;
	private JTextField numOfEmptyDocksField;
	
	public StationAddDialog(Core core, CoreManageController controller) {
		super(core, controller);
		System.out.println("StationAddDialog controller = " + controller);
	}
	
	@Override
	public void buildControls() {
		super.buildControls();
		
		if (t instanceof Station) {
			Station book = (Station) t;
			
			int row = getLastRowIndex();
			
			JLabel mfLabel = new JLabel("Address");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(mfLabel, c);
			addressField = new JTextField(15);
			add(mfLabel, c);
			c.gridx = 1;
			c.gridy = row;
			add(addressField, c);
			row = getLastRowIndex();
			
			JLabel licenseLabel = new JLabel("NumOfBikes");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(licenseLabel, c);
			numOfBikesField = new JTextField(15);
			add(licenseLabel, c);
			c.gridx = 1;
			c.gridy = row;
			add(numOfBikesField, c);
			row = getLastRowIndex();
			
			JLabel costLabel = new JLabel("NumOfTwinBikes");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(costLabel, c);
			numOfTwinBikesField = new JTextField(15);
			add(costLabel, c);
			c.gridx = 1;
			c.gridy = row;
			add(numOfTwinBikesField, c);
			row = getLastRowIndex();
			
			JLabel producerLabel = new JLabel("NumOfEBikes");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(producerLabel, c);
			numOfEBikesField = new JTextField(15);
			add(producerLabel, c);
			c.gridx = 1;
			c.gridy = row;
			add(numOfEBikesField, c);
			
			
			row = getLastRowIndex();
			JLabel languageLabel = new JLabel("NumOfEmptyDocks");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(languageLabel, c);
			numOfEmptyDocksField = new JTextField(15);
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(numOfEmptyDocksField, c);
		}
	}

	@Override
	public Core createNewData() {
		super.createNewData();
		
		if (t instanceof Station) {
			Station station = (Station) t;
			
			station.setAddress(addressField.getText());
			station.setNumberOfBikes(Integer.parseInt(numOfBikesField.getText()));
			station.setNumberOfEBikes(Integer.parseInt(numOfEBikesField.getText()));
			station.setNumberOfTwinBikes(Integer.parseInt(numOfTwinBikesField.getText()));
			station.setNumberOfEmptyDocks(Integer.parseInt(numOfEmptyDocksField.getText()));
		}
		
		return t;
	}
}