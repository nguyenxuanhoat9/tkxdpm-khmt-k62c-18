package com.oms.components.core.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.oms.bean.*;

import com.oms.components.abstractdata.gui.ADataAddDialog;

import com.oms.components.abstractdata.controller.CoreManageController;

@SuppressWarnings("serial")
public class CoreAddPane extends JPanel {

	protected GridBagLayout layout;
	protected GridBagConstraints c;
	
	private Core core = new Core();
	protected CoreManageController controller;

	public CoreAddPane(CoreManageController controller) {
		this.controller = controller;
		layout = new GridBagLayout();
		this.setLayout(layout);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
		
		int row = getLastRowIndex();
		c.gridx = 2;
		c.gridy = row - 1;
		JButton searchButton = new JButton("Add");
		add(searchButton, c);
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				 display();
			}
		});
		

		// Empty label for resizing
		c.weightx = 1;
		c.gridx = 3;
		c.gridy = row - 1;
		add(new JLabel(), c);
	}
	
	protected int getLastRowIndex() {
		layout.layoutContainer(this);
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
	
//	public CoreAddDialog display() {
//		return new CoreAddDialog(controller);
//	}
	public ADataAddDialog display() {
		return new CoreAddDialog(core,controller);
	}
}
