package com.oms.components.core.station.controller;

import com.oms.bean.Bike;
import com.oms.bean.Core;
import com.oms.bean.Station;
import com.oms.components.abstractdata.controller.CoreManageController;
import com.oms.components.core.controller.AAdminCorePageController;
import com.oms.components.core.gui.CoreSearchPane;
import com.oms.components.core.gui.CoreSinglePane;
import com.oms.components.core.gui.CoreViewDialog;
import com.oms.components.core.station.gui.StationSearchPane;
import com.oms.components.core.station.gui.StationSinglePane;
import com.oms.components.core.station.gui.StationViewDialog;
import com.oms.components.core.station.gui.StationAddPane;
import com.oms.components.core.gui.BikeAddPane;
import com.oms.components.core.gui.CoreAddPane;
import com.oms.serverapi.CoreApi;
import java.util.List;
import java.util.Map;

public class AdminStationPageController extends AAdminCorePageController {
//
//	public CoreManageController controller;
//	
//	public AdminStationPageController() {
//		this.controller =  new CoreManageController(this);
		
//	}
	
	@Override
	public List<? extends Core> search(Map<String, String> searchParams) {
		return new CoreApi().getStations(searchParams);
	}
	
	@Override
	public CoreSinglePane createSinglePane() {
		return new StationSinglePane();
	}
	
	@Override
	public CoreSearchPane createSearchPane() {
		return new StationSearchPane();
	}
	
	public CoreAddPane createAddPane() {
		CoreManageController controller = new CoreManageController(this);
		return new StationAddPane(controller);
	}
        
	@Override
	public Core updateCore(Core core) {
		return new CoreApi().updateStations((Station) core);
	}
	
	public Core createCore(Core core) {
		return new CoreApi().createStation((Station) core);
	}
//	public Station getStationById(String searchParams) {
//		return new CoreApi().getStationById(searchParams);
//	}
	
}
