package com.oms.components.core.station.controller;

import com.oms.bean.Core;
import com.oms.bean.Station;
import com.oms.components.core.controller.UserCorePageController;
import com.oms.components.core.gui.CoreSearchPane;
import com.oms.components.core.gui.CoreSinglePane;
import com.oms.components.core.gui.CoreViewDialog;
import com.oms.components.core.station.gui.StationSearchPane;
import com.oms.components.core.station.gui.StationSinglePane;
import com.oms.components.core.station.gui.StationViewDialog;
import com.oms.components.core.gui.CoreAddPane;
import com.oms.serverapi.CoreApi;
import java.util.List;
import java.util.Map;

public class UserStationPageController extends UserCorePageController {

	@Override
	public List<? extends Core> search(Map<String, String> searchParams) {
		return new CoreApi().getStations(searchParams);
	}
	
	@Override
	public CoreSinglePane createSinglePane() {
		return new StationSinglePane();
	}
	
	@Override
	public CoreSearchPane createSearchPane() {
		return new StationSearchPane();
	}
        
	@Override
	public Core updateCore(Core core) {
		return new CoreApi().updateStations((Station) core);
	}
	
	public Core createCore(Core core) {
		return new CoreApi().createStation((Station) core);
	}
	public Station getStationById(String searchParams) {
		return new CoreApi().getStationById(searchParams);
	}
	
}
