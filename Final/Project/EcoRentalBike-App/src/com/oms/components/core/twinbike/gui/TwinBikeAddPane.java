package com.oms.components.core.twinbike.gui;

import com.oms.bean.EBike;
import com.oms.bean.TwinBike;
import com.oms.components.abstractdata.controller.CoreManageController;
import com.oms.components.abstractdata.gui.ADataAddDialog;
import com.oms.components.core.gui.BikeAddDialog;
import com.oms.components.core.gui.CoreAddPane;

public class TwinBikeAddPane extends CoreAddPane {

private TwinBike tbike;
	
	private CoreManageController controller;
	
	public TwinBikeAddPane(CoreManageController controller) {
		super(controller);
	}
	
	@Override
	public ADataAddDialog display() {
		return new TwinBikeAddDialog(tbike ,controller);
	}
	
}
