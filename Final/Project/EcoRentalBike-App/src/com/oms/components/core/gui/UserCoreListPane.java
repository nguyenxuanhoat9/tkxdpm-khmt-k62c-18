package com.oms.components.core.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.oms.bean.Core;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.UserListPane;
import com.oms.components.abstractdata.gui.ADataSinglePane;
import com.oms.components.abstractdata.controller.AUserPageController;
import com.oms.components.core.controller.UserCorePageController;
import com.oms.components.core.station.gui.StationViewDialog;

public class UserCoreListPane extends UserListPane<Core> {

	public UserCoreListPane(AUserPageController<Core> controller) {
		this.controller = controller;
	}
	
	@Override
	public void decorateSinglePane(ADataSinglePane<Core> singlePane) {
		JButton button = new JButton("View");
		singlePane.addDataHandlingComponent(button);
		
		IDataManageController<Core> manageController = new IDataManageController<Core>() {
			@Override
			public void update(Core t) {
				if (controller instanceof UserCorePageController) {
					Core newCore = ((UserCorePageController) controller).updateCore(t);
					singlePane.updateData(newCore);
				}
			}

			@Override
			public void create(Core t) {
			}

			@Override
			public void read(Core t) {
			}

			@Override
			public void delete(Core t) {
				
			}
		};
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new StationViewDialog(singlePane.getData(), manageController, "Station detail");
			}
		});	
	}
}
