package com.oms.app.user;

import javax.swing.JPanel;

import com.oms.bean.Core;
import com.oms.components.abstractdata.controller.AUserPageController;
import com.oms.components.core.bike.controller.AdminBikePageController;
import com.oms.components.core.ebike.controller.AdminEBikePageController;
import com.oms.components.core.twinbike.controller.AdminTwinBikePageController;
import com.oms.components.core.station.controller.UserStationPageController;

public class OMSUserController {

	public OMSUserController() {
	}
	
	
	
	public JPanel getStationPage() {
		AUserPageController<Core> controller = new UserStationPageController();
		return controller.getDataPagePane();
	}
}
