package com.oms.app.user;

import java.awt.BorderLayout;

import javax.swing.*;

@SuppressWarnings("serial")
public class OMSUser extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;

	public OMSUser(OMSUserController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);

		
		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);
		
//		
//		JPanel bikePage = controller.getBikePage();
//		tabbedPane.addTab("Bikes", null, bikePage, "Bikes");
//		
//		JPanel ebikePage = controller.getEBikePage();
//		tabbedPane.addTab("EBikes", null, ebikePage, "EBikes");
//		
//		JPanel tbikePage = controller.getTwinBikePage();
//		tabbedPane.addTab("TwinBikes", null, tbikePage, "TwinBikes");
		
		JPanel stationPage = controller.getStationPage();
//		tabbedPane.addTab("Stations", null, stationPage, "Stations");
                
                rootPanel.add(stationPage);
		

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("User home page");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new OMSUser(new OMSUserController());
			}
		});
	}
}
